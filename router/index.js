const express = require('express');
const router = express.Router(); // Usamos express.Router() para crear un enrutador
const path = require('path');

// Ruta principal
router.get('/', (req, res) => {
    // Lógica para calcular los pagos, impuestos y bonos
    const maestros = [
        { numDocente: 1, nombre: 'Juan', domicilio: 'Calle 123', nivel: 1, pagoBase: 100, horas: 40, hijos: 2 },
        // Agrega más maestros aquí
    ];

    // Función para calcular el pago base
    const calcularPagoBase = (nivel, pagoBase) => {
        let porcentajeIncremento;
        switch (nivel) {
            case 1:
                porcentajeIncremento = 0.3;
                break;
            case 2:
                porcentajeIncremento = 0.5;
                break;
            case 3:
                porcentajeIncremento = 1.0;
                break;
            default:
                porcentajeIncremento = 0;
        }
        return pagoBase * (1 + porcentajeIncremento);
    };

    // Función para calcular el impuesto
    const calcularImpuesto = (pagoTotal) => {
        return pagoTotal * 0.16;
    };

    // Función para calcular el bono
    const calcularBono = (cantidadHijos) => {
        if (cantidadHijos <= 2) {
            return 0.05;
        } else if (cantidadHijos >= 3 && cantidadHijos <= 5) {
            return 0.1;
        } else {
            return 0.2;
        }
    };

    // Calcular pagos, impuestos y bonos para cada maestro
    const resultados = maestros.map((maestro) => {
        const pagoBase = calcularPagoBase(maestro.nivel, maestro.pagoBase);
        const pagoTotal = pagoBase * maestro.horas;
        const impuesto = calcularImpuesto(pagoTotal);
        const bono = calcularBono(maestro.hijos);
        return {
            ...maestro,
            pagoTotal: pagoTotal,
            impuesto: impuesto,
            bono: bono * pagoTotal
        };
    });

    // Renderizar la vista con los resultados
    res.render('index', { resultados: resultados });
});

module.exports = router; // Exportamos el router
