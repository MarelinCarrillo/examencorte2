const express = require('express');
const bodyParser = require('body-parser');
const path = require('path');

const myRoutes = require('./router/index.js');

const puerto = 3000; // Cambiado el puerto para evitar conflictos

// Crear la aplicación Express
const main = express();

// Configuración de la vista y los archivos estáticos
main.set('view engine', 'ejs');
main.use(express.static(path.join(__dirname, 'public')));

// Middleware para analizar cuerpos de solicitud entrantes
main.use(bodyParser.urlencoded({ extended: true }));

// Usar las rutas definidas en el archivo router/index.js
main.use(myRoutes);

main.listen(puerto, () => {
  console.log(`Servidor corriendo en http://localhost:${puerto}`);
});
